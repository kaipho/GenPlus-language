'use strict';

import * as net from 'net';
import * as path from 'path';

import { workspace, Disposable, ExtensionContext } from 'vscode';
import { LanguageClient, LanguageClientOptions, ServerOptions } from 'vscode-languageclient';

export function activate(context: ExtensionContext) {
	// The server is a locally installed Java application
    const executablExt = process.platform == 'win32' ? '.bat' : '';
	const executable = 'xtext-genplus-server' + executablExt;
    const command = context.asAbsolutePath(path.join('xtext-genplus-server', 'bin', executable));
    const serverOptions = { command };
    
    // Options to control the language client
    const clientOptions: LanguageClientOptions = {
        // Register the server for plain text documents
        documentSelector: ['genplus'],
        synchronize: {
            configurationSection: 'genplusVsCode',
            // Notify the server about file changes to '.clientrc files contain in the workspace
            fileEvents: workspace.createFileSystemWatcher('**/*.*')
        }
    }
	
	// Create the language client and start the client.
	const disposable = new LanguageClient('Xtext Server', serverOptions, clientOptions).start();
	
	// Push the disposable to the context's subscriptions so that the 
	// client can be deactivated on extension deactivation
	context.subscriptions.push(disposable);
}
