[![pipeline status](https://gitlab.com/kaipho/GenPlus-language/badges/master/pipeline.svg)](https://gitlab.com/kaipho/GenPlus-language/commits/master)
[![](http://vsmarketplacebadge.apphb.com/version/kaipho.genplus.svg)](https://marketplace.visualstudio.com/items?itemName=kaipho.genplus)


ANTLR language and LSP support for gen+. Build with XText.

Download latest jar artifacts: https://gitlab.com/kaipho/GenPlus-language/-/jobs/artifacts/master/browse?job=build