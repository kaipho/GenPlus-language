package de.kaipho.genplus_vscode.ide

import com.google.inject.Guice
import de.kaipho.genplus_vscode.GenplusRuntimeModule
import de.kaipho.genplus_vscode.GenplusStandaloneSetup

/**
 * Initialization support for running Xtext languages without Equinox extension registry.
 */
class GenplusIdeSetup extends GenplusStandaloneSetup {

	override createInjector() {
		Guice.createInjector(new GenplusRuntimeModule, new GenplusIdeModule)
	}
}
