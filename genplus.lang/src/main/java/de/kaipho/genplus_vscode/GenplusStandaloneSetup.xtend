package de.kaipho.genplus_vscode


/**
 * Initialization support for running Xtext languages without Equinox extension registry.
 */
class GenplusStandaloneSetup extends GenplusStandaloneSetupGenerated {

	def static void doSetup() {
		new GenplusStandaloneSetup().createInjectorAndDoEMFRegistration()
	}
}
