package de.kaipho.genplus_vscode


/**
 * Use this class to register components to be used at runtime / without the Equinox extension registry.
 */
class GenplusRuntimeModule extends AbstractGenplusRuntimeModule {
}
